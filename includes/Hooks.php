<?php

namespace MediaWiki\Extension\VueSSRProvider;
use Config;
use IContextSource;
use MediaWiki\Config\ServiceOptions;
use ResourceLoader;

class Hooks {
	public static function onRegistration() {
		global $wgVueRendererProviders, $wgVueRendererProvider;

		$wgVueRendererProviders['VueSSRProvider'] = [
			'class' => 'MediaWiki\\Extension\\VueSSRProvider\\SSRVueRenderer',
			'factory' => static function ( IContextSource $context, ResourceLoader $resourceLoader, Config $config ) {
				$options = new ServiceOptions( SSRVueRenderer::CONSTRUCTOR_OPTIONS, $config );
				return new SSRVueRenderer( $options, $context, $resourceLoader );
			},
			'services' => [ 'ResourceLoader', 'MainConfig' ]
		];

		$wgVueRendererProvider = 'VueSSRProvider';
	}
}
