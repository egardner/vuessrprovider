<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @author Roan Kattouw
 */

namespace MediaWiki\Extension\VueSSRProvider;

use Exception;
use MediaWiki\Config\ServiceOptions;
use MediaWiki\ResourceLoader\VueRenderer;
use MultiHttpClient;
use Html;
use IContextSource;
use MediaWiki\ResourceLoader\ResourceLoader;
use MediaWiki\ResourceLoader\Context;
use MediaWiki\ResourceLoader\Module;
use Xml;

class SSRVueRenderer extends VueRenderer {
	public const CONSTRUCTOR_OPTIONS = [
		'SSRVueRendererServerUrl'
	];

	/** @var MultiHttpClient */
	private $http;

	public static $implicitModules = [
		'vue'
	];

	public function __construct( ServiceOptions $options, IContextSource $context, ResourceLoader $resourceLoader ) {
		parent::__construct( $context, $resourceLoader );
		$options->assertRequiredOptions( self::CONSTRUCTOR_OPTIONS );
		$this->serverUrl = $options->get( 'SSRVueRendererServerUrl' );
		$this->http = new MultiHttpClient( [] );
	}

	public function renderModule( string $moduleName, string $id, array $options = [] ) {
		$baseOptions = [
			'props' => $options['props'] ?? [],
			'exportProperty' => $options['exportProperty'] ?? null,
		];

		$rlContext = $this->getResourceLoaderContext();

		$modules = $this->getModulesWithDependencies( $moduleName );
		foreach ( $modules as &$module ) {
			$module = $this->getModuleOutput( $module, $rlContext );
		}

		$response = $this->http->run( [
			'method' => 'POST',
			'url' => $this->serverUrl,
			'headers' => [
				'Content-Type' => 'application/json'
			],
			'body' => json_encode( $baseOptions + [
				'modules' => $modules,
				'mainModule' => $moduleName,
				'lang' => $rlContext->getLanguage()
			] )
		] );
		$result = json_decode( $response['body'], true );

		if ( $result && isset( $result['html'] ) ) {
			$wrapperHtml = $result['html'];
			$mountOptions = $baseOptions + [ 'createSSRApp' => true ];
			$extraScript = '';
		} else {
			$wrapperHtml = $options['placeholderHtml'] ?? '';
			$mountOptions = $baseOptions + [ 'createSSRApp' => false ];

			$error = $result['error'] ?? $response['error'] ?? $response['reason'];
			$extraScript = Xml::encodeJsCall(
				'console.warn',
				[ "Vue server rendering failed for $moduleName #$id: $error" ]
			);
		}

		return [
			'html' => Html::rawElement( 'div', [ 'id' => $id ], $wrapperHtml ),
			'script' => ResourceLoader::makeInlineCodeWithModule(
				[ 'vue', $moduleName ],
				Xml::encodeJsCall(
					'mw.mountVueComponent',
					[ $moduleName, $id, $mountOptions ]
				)
			) . $extraScript,
			'allModules' => array_keys( $modules ), // TODO standardize
		];
	}

	protected function getModulesWithDependencies( string $mainModuleName ): array {
		$gatheredModules = [ $mainModuleName => $this->getResourceLoader()->getModule( $mainModuleName ) ];
		$resolutionQueue = [ $mainModuleName ];
		while ( $resolutionQueue ) {
			$moduleName = array_shift( $resolutionQueue );
			foreach ( $gatheredModules[ $moduleName ]->getDependencies() as $dependency ) {
				if ( !isset( $gatheredModules[ $dependency ] ) && !in_array( $dependency, self::$implicitModules ) ) {
					$gatheredModules[ $dependency ] = $this->getResourceLoader()->getModule( $dependency );
					$resolutionQueue[] = $dependency;
				}
			}
		}
		return $gatheredModules;
	}

	protected function getModuleOutput( Module $module, Context $rlContext ) {
		$dependencies = $module->getDependencies( $rlContext );
		$moduleContent = $module->getModuleContent( $rlContext );
		$script = $moduleContent['scripts'];
		if ( is_string( $script ) ) {
			// Convert non-package modules to package structure
			$output = [
				'files' => [
					'index.js' => $script
				],
				'entry' => 'index.js',
			];
		} else {
			// For package modules, change 'main' to 'entry' and unwrap file contents
			$output = [
				'files' => array_map( static function ( $s ) {
					return $s['content'];
				}, $script['files'] ),
				'entry' => $script['main']
			];
		}
		$output += [
			'messages' => $moduleContent['messagesBlob'] ?? '{}',
			'dependencies' => $dependencies
		];
		return $output;
	}
}