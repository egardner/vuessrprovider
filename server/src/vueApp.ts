import { createSSRApp, ComponentOptions } from 'vue';
import { renderToString } from 'vue/server-renderer';
import makeI18nPlugin from './i18nPlugin';
import { RequestExecutionContext } from './rlModule';

export async function renderApp(
	component: ComponentOptions<never>,
	props: Record<string, unknown>,
	requestContext: RequestExecutionContext
) : Promise<string> {
	const app = createSSRApp( component, props );
	app.use( makeI18nPlugin( requestContext.mockWindow ) );

	return renderToString( app );
}
