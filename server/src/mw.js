module.exports = ( function ( userLanguage ) {

	// The code below is copied verbatim from resources/src/mediawiki.base/mediawiki.base.js
	// in MediaWiki core, with minimal modifications:
	// - mw.messages is initialized separately
	// - In Message.prototype.parser, mw.config.get( 'wgUserLanguage' ) is changed to userLanguage
	// - In Message.prototype.exists, this.map.exists is changed to this.map.has (TODO undo this)

	var slice = Array.prototype.slice;

	mw = {
		messages: new Map()
	};

	/**
	 * Replace `$*` with a list of parameters for `uselang=qqx` support.
	 *
	 * @private
	 * @since 1.33
	 * @param {string} formatString Format string
	 * @param {Array} parameters Values for $N replacements
	 * @return {string} Transformed format string
	 */
	mw.internalDoTransformFormatForQqx = function ( formatString, parameters ) {
		var replacement;
		if ( formatString.indexOf( '$*' ) !== -1 ) {
			replacement = '';
			if ( parameters.length ) {
				replacement = ': ' + parameters.map( function ( _, i ) {
					return '$' + ( i + 1 );
				} ).join( ', ' );
			}
			return formatString.replace( '$*', replacement );
		}
		return formatString;
	};

	/**
	 * Format a string. Replace $1, $2 ... $N with positional arguments.
	 *
	 * Used by Message#parser().
	 *
	 * @since 1.25
	 * @param {string} formatString Format string
	 * @param {...Mixed} parameters Values for $N replacements
	 * @return {string} Formatted string
	 */
	mw.format = function ( formatString ) {
		var parameters = slice.call( arguments, 1 );
		formatString = mw.internalDoTransformFormatForQqx( formatString, parameters );
		return formatString.replace( /\$(\d+)/g, function ( str, match ) {
			var index = parseInt( match, 10 ) - 1;
			return parameters[ index ] !== undefined ? parameters[ index ] : '$' + match;
		} );
	};

	/**
	 * HTML construction helper functions
	 *
	 *     @example
	 *
	 *     var Html, output;
	 *
	 *     Html = mw.html;
	 *     output = Html.element( 'div', {}, new Html.Raw(
	 *         Html.element( 'img', { src: '<' } )
	 *     ) );
	 *     mw.log( output ); // <div><img src="&lt;"/></div>
	 *
	 * @class mw.html
	 * @singleton
	 */

	function escapeCallback( s ) {
		switch ( s ) {
			case '\'':
				return '&#039;';
			case '"':
				return '&quot;';
			case '<':
				return '&lt;';
			case '>':
				return '&gt;';
			case '&':
				return '&amp;';
		}
	}
	mw.html = {
		/**
		 * Escape a string for HTML.
		 *
		 * Converts special characters to HTML entities.
		 *
		 *     mw.html.escape( '< > \' & "' );
		 *     // Returns &lt; &gt; &#039; &amp; &quot;
		 *
		 * @param {string} s The string to escape
		 * @return {string} HTML
		 */
		escape: function ( s ) {
			return s.replace( /['"<>&]/g, escapeCallback );
		},

		/**
		 * Create an HTML element string, with safe escaping.
		 *
		 * @param {string} name The tag name.
		 * @param {Object} [attrs] An object with members mapping element names to values
		 * @param {string|mw.html.Raw|null} [contents=null] The contents of the element.
		 *
		 *  - string: Text to be escaped.
		 *  - null: The element is treated as void with short closing form, e.g. `<br/>`.
		 *  - this.Raw: The raw value is directly included.
		 * @return {string} HTML
		 */
		element: function ( name, attrs, contents ) {
			var v, attrName, s = '<' + name;

			if ( attrs ) {
				for ( attrName in attrs ) {
					v = attrs[ attrName ];
					// Convert name=true, to name=name
					if ( v === true ) {
						v = attrName;
						// Skip name=false
					} else if ( v === false ) {
						continue;
					}
					s += ' ' + attrName + '="' + this.escape( String( v ) ) + '"';
				}
			}
			if ( contents === undefined || contents === null ) {
				// Self close tag
				s += '/>';
				return s;
			}
			// Regular open tag
			s += '>';
			if ( typeof contents === 'string' ) {
				// Escaped
				s += this.escape( contents );
			} else if ( typeof contents === 'number' || typeof contents === 'boolean' ) {
				// Convert to string
				s += String( contents );
			} else if ( contents instanceof this.Raw ) {
				// Raw HTML inclusion
				s += contents.value;
			} else {
				throw new Error( 'Invalid content type' );
			}
			s += '</' + name + '>';
			return s;
		},

		/**
		 * Wrapper object for raw HTML passed to mw.html.element().
		 *
		 * @class mw.html.Raw
		 * @constructor
		 * @param {string} value
		 */
		Raw: function ( value ) {
			this.value = value;
		}
	};


	/**
	 * Object constructor for messages.
	 *
	 * Similar to the Message class in MediaWiki PHP.
	 *
	 *     @example
	 *
	 *     var obj, str;
	 *     mw.messages.set( {
	 *         'hello': 'Hello world',
	 *         'hello-user': 'Hello, $1!',
	 *         'welcome-user': 'Welcome back to $2, $1! Last visit by $1: $3',
	 *         'so-unusual': 'You will find: $1'
	 *     } );
	 *
	 *     obj = mw.message( 'hello' );
	 *     mw.log( obj.text() );
	 *     // Hello world
	 *
	 *     obj = mw.message( 'hello-user', 'John Doe' );
	 *     mw.log( obj.text() );
	 *     // Hello, John Doe!
	 *
	 *     obj = mw.message( 'welcome-user', 'John Doe', 'Wikipedia', '2 hours ago' );
	 *     mw.log( obj.text() );
	 *     // Welcome back to Wikipedia, John Doe! Last visit by John Doe: 2 hours ago
	 *
	 *     // Using mw.msg shortcut, always in "text' format.
	 *     str = mw.msg( 'hello-user', 'John Doe' );
	 *     mw.log( str );
	 *     // Hello, John Doe!
	 *
	 *     // Different formats
	 *     obj = mw.message( 'so-unusual', 'Time "after" <time>' );
	 *
	 *     mw.log( obj.text() );
	 *     // You will find: Time "after" <time>
	 *
	 *     mw.log( obj.escaped() );
	 *     // You will find: Time &quot;after&quot; &lt;time&gt;
	 *
	 * @class mw.Message
	 *
	 * @constructor
	 * @param {mw.Map} map Message store
	 * @param {string} key
	 * @param {Array} [parameters]
	 */
	function Message( map, key, parameters ) {
		this.map = map;
		this.key = key;
		this.parameters = parameters || [];
	}

	Message.prototype = {
		/**
		 * Get parsed contents of the message.
		 *
		 * The default parser does simple $N replacements and nothing else.
		 * This may be overridden to provide a more complex message parser.
		 * The primary override is in the mediawiki.jqueryMsg module.
		 *
		 * This function will not be called for nonexistent messages.
		 *
		 * @private For internal use by mediawiki.jqueryMsg only
		 * @param {string} format
		 * @return {string} Parsed message
		 */
		parser: function ( format ) {
			var text = this.map.get( this.key );
			if (
				userLanguage === 'qqx' &&
				text === '(' + this.key + ')'
			) {
				text = '(' + this.key + '$*)';
			}
			text = mw.format.apply( null, [ text ].concat( this.parameters ) );
			if ( format === 'parse' ) {
				// We don't know how to parse anything, so escape it all
				text = mw.html.escape( text );
			}
			return text;
		},

		/**
		 * Add (does not replace) parameters for `$N` placeholder values.
		 *
		 * @param {Array} parameters
		 * @return {mw.Message}
		 * @chainable
		 */
		params: function ( parameters ) {
			var i;
			for ( i = 0; i < parameters.length; i++ ) {
				this.parameters.push( parameters[ i ] );
			}
			return this;
		},

		/**
		 * Convert message object to a string using the "text"-format .
		 *
		 * This exists for implicit string type casting only.
		 * Do not call this directly. Use mw.Message#text() instead, one of the
		 * other format methods.
		 *
		 * @private
		 * @param {string} [format="text"] Internal parameter. Uses "text" if called
		 *  implicitly through string casting.
		 * @return {string} Message in the given format, or `⧼key⧽` if the key
		 *  does not exist.
		 */
		toString: function ( format ) {
			if ( !this.exists() ) {
				// Use ⧼key⧽ as text if key does not exist
				// Err on the side of safety, ensure that the output
				// is always html safe in the event the message key is
				// missing, since in that case its highly likely the
				// message key is user-controlled.
				// '⧼' is used instead of '<' to side-step any
				// double-escaping issues.
				// (Keep synchronised with Message::toString() in PHP.)
				return '⧼' + mw.html.escape( this.key ) + '⧽';
			}

			if ( !format ) {
				format = 'text';
			}

			if ( format === 'plain' || format === 'text' || format === 'parse' ) {
				return this.parser( format );
			}

			// Format: 'escaped' (including for any invalid format, default to safe escape)
			return mw.html.escape( this.parser( 'escaped' ) );
		},

		/**
		 * Parse message as wikitext and return HTML.
		 *
		 * If jqueryMsg is loaded, this transforms text and parses a subset of supported wikitext
		 * into HTML. Without jqueryMsg, it is equivalent to #escaped.
		 *
		 * @return {string} String form of parsed message
		 */
		parse: function () {
			return this.toString( 'parse' );
		},

		/**
		 * Return message plainly.
		 *
		 * This substitutes parameters, but otherwise does not transform the
		 * message content.
		 *
		 * @return {string} String form of plain message
		 */
		plain: function () {
			return this.toString( 'plain' );
		},

		/**
		 * Format message with text transformations applied.
		 *
		 * If jqueryMsg is loaded, `{{`-transformation is done for supported
		 * magic words such as `{{plural:}}`, `{{gender:}}`, and `{{int:}}`.
		 * Without jqueryMsg, it is equivalent to #plain.
		 *
		 * @return {string} String form of text message
		 */
		text: function () {
			return this.toString( 'text' );
		},

		/**
		 * Format message and return as escaped text in HTML.
		 *
		 * This is equivalent to the #text format, which is then HTML-escaped.
		 *
		 * @return {string} String form of html escaped message
		 */
		escaped: function () {
			return this.toString( 'escaped' );
		},

		/**
		 * Check if a message exists
		 *
		 * @see mw.Map#exists
		 * @return {boolean}
		 */
		exists: function () {
			// return this.map.exists( this.key );
			// TODO support mw.Map
			return this.map.has( this.key );
		}
	};

	// Expose Message constructor
	mw.Message = Message;

	/**
	 * Get a message object.
	 *
	 * Shortcut for `new mw.Message( mw.messages, key, parameters )`.
	 *
	 * @see mw.Message
	 * @param {string} key Key of message to get
	 * @param {...Mixed} parameters Values for $N replacements
	 * @return {mw.Message}
	 */
	mw.message = function ( key ) {
		var parameters = slice.call( arguments, 1 );
		return new Message( mw.messages, key, parameters );
	};

	/**
	 * Get a message string using the (default) 'text' format.
	 *
	 * Shortcut for `mw.message( key, parameters... ).text()`.
	 *
	 * @see mw.Message
	 * @param {string} key Key of message to get
	 * @param {...Mixed} parameters Values for $N replacements
	 * @return {string}
	 */
	mw.msg = function () {
		// Shortcut must process text transformations by default
		// if mediawiki.jqueryMsg is loaded. (T46459)
		return mw.message.apply( mw, arguments ).text();
	};

	return mw;
} );
