import express from 'express';
import { renderToString } from 'vue/server-renderer';
import { executeModule, makeRequestContext, ModuleDefinition } from './rlModule';
import { renderApp } from './vueApp';

interface RequestData {
	modules: Record<string, ModuleDefinition>,
	lang: string,
	mainModule?: string,
	exportProperty?: string,
	props: Record<string, unknown>,
	attrs: Record<string, unknown>
}

const server = express();
server.use( express.json( { limit: '10MB' } ) );

server.post( '/render', async ( req, res ) => {
	const {
		modules,
		lang,
		mainModule = 'main',
		exportProperty = null,
		props = {}
	} = ( req.body as RequestData );

	const requestContext = makeRequestContext( modules, lang );
	const mainExport = executeModule( requestContext, mainModule );
	const componentObject = exportProperty === null ? mainExport : mainExport[ exportProperty ];

	try {
		const html = await renderApp( componentObject, props, requestContext );
		res.json( {
			html
		} );
	} catch ( error ) {
		// TODO serialize exception and somehow render it in the console on the client?
		res.json( { error: error } );
		// FIXME console.warn instead
		throw error;
	}
} );

server.listen( 8082 );
console.log( 'Listening on port 8082' );
