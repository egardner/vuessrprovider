# VueSSRProvider

Prototype MediaWiki extension that provides Vue SSR support

This uses the SSRProvider feature in MediaWiki core, which is prototyped here:
https://gerrit.wikimedia.org/r/c/mediawiki/core/+/779971

## Installation

- In your MediaWiki installation, download [the SSRProvider patch](https://gerrit.wikimedia.org/r/c/mediawiki/core/+/779971)
  (using `git review -d 779971`)
- At the bottom of `LocalSettings.php`, add `wfLoadExtension( 'VueSSRProvider' );`
    - If you're using MediaWiki-Vagrant, add a file to the `vagrant/settings.d` directory with the
	  contents `<?php wfLoadExtension( 'VueSSRProvider' );`
- Place this repo in the `extensions/VueSSRProvider` directory
- In the `extensions/VueSSRProvider/server` directory, run `npm install`, then `npm start`
    - If you're using MediaWiki-Vagrant, you'll need to either run the `npm start` command inside
	  the Vagrant VM, or make the server accessible to Vagrant by running
	  `vagrant ssh -- -R 8082:localhost:8082` and keeping that shell open
- To test, install the VueTest extension, download [the SSR demo patch](https://gerrit.wikimedia.org/r/c/mediawiki/extensions/VueTest/+/787825) (using `git review -d 787825`) and go to the `Special:VueTest` page on your wiki
